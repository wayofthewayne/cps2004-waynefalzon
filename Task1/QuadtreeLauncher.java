import java.io.*;

//This is the driver class 
public class QuadtreeLauncher{
	public static void main(String [] args){

		 QuadTree tree = new QuadTree(); //Creating quadtree instance 
     Functions func = new Functions(); //Creating function instance 
      
     File filename = new File("8by8.txt"); //creating file instance 
     func.ReadFile(tree,filename); //processing 
     
     TestDisplay(tree, tree.size); //Function call to display the tree for testing
   }
 

   /*tree: is the quadtree to be displayed  
    size: is the size of this tree
    Function displays the nodes of the tree. It will indicate the level and the regions
    Regions are denoted as 0 (NW) 1(NE) 2 (SE) 3 (SW) 
   */
   public static void TestDisplay(QuadTree tree, int size){

        int caseNo=5; //set to any number which displays the defualt case 

        if(size == 2){ //using the size we set the case number to trigger the appropraite case 
          caseNo = 0; 
        }else if(size == 4){
          caseNo = 1;
        }else if(size == 8){
          caseNo = 2;
        }

       switch(caseNo){ //The switch will determine which size is to be printed 

        case 0: //for 2 by 2

          for(int i=0; i<4;i++){
            System.out.println(tree.child[i].quadNode.leaf.colour);
          }
        break;

        case 1: // for 4 by 4 

          for(int i=0; i<4; i++){
              System.out.println("\nOuter region (level 1): " + i + "\n");
            for(int j=0; j<4; j++){
                System.out.println(tree.child[i].child[j].quadNode.leaf.colour+ " ");
            }
          }   
        break;

        case 2: //for 8 by 8 

          for(int i=0; i<4; i++){
            System.out.println();
            System.out.println("\nOuter Region (level 1): " + i + "\n");
              for(int j=0; j<4; j++){
                 System.out.println("\nInner region (level 2): " + j);
                   for(int k=0; k<4; k++){
                       System.out.print(tree.child[i].child[j].child[k].quadNode.leaf.colour + " ");
                    }   
              }
          }
        System.out.println();
        break;

        default:
         System.out.println("Unable to support this size of tree"); 
       }
   }

}


