//Will be found at the end of tree. 
//store information in the tree. (is colour black of white/ true of false)
public class Leaf{

	public boolean colour;//will indicate whether the bit/text is black or white 

    //parameterised constructor 
    Leaf(boolean colour){//sets the class attributes 
       this.colour = colour; 
    }

	Leaf(){//empty constructor 
	}
}