//Coordinate will be a point (indicating the position of a node)
public class Coordinate{
	public int x; //will hold the coordinates 
	public int y; 
    

    /*x = x coordinate, y = y coordinate
    Constructor sets class attributes 
    */  
	Coordinate(int x, int y){
        this.x = x;
        this.y = y;
	}

	Coordinate(){ //empty constructor 
	}
}