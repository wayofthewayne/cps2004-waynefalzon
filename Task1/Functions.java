import java.io.*;

//This class will contain several functions for several different purposes 
public class Functions{

     /*tree = this is the quadtree to processed
      filename = this is the file to be read 
     Function will open the file and start to process line by line 
     */
 	public static void ReadFile(QuadTree tree, File filename){  
          BufferedReader reader; 

         try{
            
           reader = new BufferedReader (new FileReader(filename));
           String line = reader.readLine(); //line is stored in string 

           Node currentNode = new Node(); 
           int yCounter=0; //will keep line count & acts as y coordinate for the node (text file case)
           boolean format = false; //identify the format to be used 

           char check = line.charAt(yCounter);  //here yCounter is used to identify the first character 

           if(Character.compare(check, 'T') == 0|| Character.compare(check, 'F') == 0){ //checking for csv or text file 
           	 format = true;
           }else{
           	 format = false; 
           }


           while(line != null){ //Till all the file is processed 


               if(format == true){
                   yCounter = ProcessString(tree,line,currentNode, yCounter);//function call (on txt file)
               }else{
                    yCounter = ProcessCSV(tree,line,currentNode,yCounter); //function call (on csv file)
               }

              line = reader.readLine(); //move to next line
           }

           reader.close(); //close file

         } catch (IOException e){ // Output errors with opening file 
           e.printStackTrace ();
         }
 	}


     //Will store contents of file to nodes (Works for textFile)
    /*tree = the quadtree to be processed 
      line = the line in the file (containing T' s & F' s) currently being considered to be stored in the node
      currentNode = the Node in question, to start being processed and stored in the tree and hold contents of line  
      yCounter = a counter which keeps tracks of the lines 
      RETRUN int, indicating the amount of lines processed so far 
    */
 	public static int ProcessString(QuadTree tree, String line, Node currentNode, int yCounter){
 		
             int xCounter = 0; //will keep count of 'line length'
             int length = line.length();

             if(xCounter == 0){	        
                tree.size = length; //setting the tree size based on the length of the line 
             }


               tree.topLeft.x = 0; //identify first topLeft corner. 
               tree.topLeft.y = 0; //This value is fixed and serves as the origin 

             do{

               currentNode.point.x = xCounter; //setting the node coordinates  
               currentNode.point.y = yCounter;

               char currentChar = line.charAt(xCounter); //reading each character 

               if(Character.compare(currentChar, 'T') == 0){ //setting node value 
               	currentNode.leaf.colour = true; 
               }else{
               	currentNode.leaf.colour = false; 
               }                  
               
               Subdivide(tree,currentNode); //Function call 
               

               xCounter++;//moving to the next character 

               if(xCounter > length-1){//when line is finished 
               	yCounter++; //move to next line 
               	xCounter=0; 
               }         

             }while(xCounter != 0); //repeat till whole line is processed 

         return yCounter;
     }
       
       //will process the csv file format and store the information in nodes 
     /*tree = this is the tree to be processed 
       line = the current line from the file (containing information in csv format) to be stored in the currentNode
      currentNode = where the lines of the file are to be stored 
      fileSizeOrContent = an integer to serve as a trigger to indicate whether we are reading the file size or actual csv content 
      RETURN int, indicating whether function has read file size or content of file 
     */
     public static int ProcessCSV(QuadTree tree, String line, Node currentNode, int fileSizeOrContent){

     	if(fileSizeOrContent == 0){ //checking if this is the first time the function is called 

     	 tree.size = Character.getNumericValue(line.charAt(fileSizeOrContent));//tree size is retreived 
     	 fileSizeOrContent++; //will indicate that next we may start to process the nodes 

     	  for(int i=0; i<tree.size; i++){//initialise tree for given size (to all false)
     	 	  currentNode.point.x = i; 

     	 	  for(int j=0; j<tree.size; j++){
              currentNode.point.y = j;
              currentNode.leaf.colour = false; 
              Subdivide(tree, currentNode);
     	 	  }
     	  }

     	  return fileSizeOrContent; 

       }else{ //reaching here would imply we have passed the first line (line containing only tree size)

        tree.topLeft.x = 0; //initialise topleft corner 
        tree.topLeft.y = 0; 
        
        currentNode.point.x = Character.getNumericValue(line.charAt(0))-1; //get node coordinates 
        currentNode.point.y = Character.getNumericValue(line.charAt(2))-1; // (-1 is used to shift from (1,1) to (0,0) coordinate system)
        currentNode.leaf.colour = true; //by definition all nodes in csv format are true 

        Subdivide(tree, currentNode); //once more we start dividing with the newly processed nodes 

        return fileSizeOrContent; 
     }
    }
    

    /*Tree = the quadtree to be processed 
      currentNode = this is the node for which we shall place in the appropraite position for the tree 
    */
      //this will recursively split the tree 
    public static void Subdivide(QuadTree tree, Node currentNode){

       if(tree.size == 1){ //Sets Node values (base case)
         tree.quadNode.leaf.colour = currentNode.leaf.colour;    
         return;
       }
       

       int retVal = tree.FindRegion(currentNode); //Returns region 

       if(retVal == 4){ //Error check 
       	 System.out.println("Region identification error!");
       	 System.exit(0);
       }


        
        if(tree.child[retVal] == null){   //if child does not exist               
           tree.child[retVal] = new QuadTree(); //create instance 
           SetBoundaries(tree.child[retVal],tree,retVal); //set it s boudaries 
        }          

        Subdivide(tree.child[retVal], currentNode);  //Call function on the child (recursive call)           
   }

    /*child = this is the child for the quadtree
      parent = this is the parent of the child 
      region = integer to indicate the quadrant 
    */
    //This will set the boundaries for a tree (serves to identify the boundaries of the children)
    public static void  SetBoundaries(QuadTree child, QuadTree parent, int region){  
     	switch(region){ //serves to identify which quadrant needs its boundary set 
        
             	case 0:  
        	       child.topLeft.x = parent.topLeft.x; //For each case set the topleft corner and the child size 
                 child.topLeft.y = parent.topLeft.y;
             	   child.size = parent.size/2;
             	break;
             	case 1:
                 child.topLeft.x = parent.topLeft.x + parent.size/2;
                 child.topLeft.y = parent.topLeft.y; 
                 child.size = parent.size/2;                          
             	break; 
             	case 2: 
                 child.topLeft.x = parent.topLeft.x + parent.size/2; 
                 child.topLeft.y = parent.topLeft.y + parent.size/2; 
                 child.size = parent.size/2;  
             	break; 
             	case 3: 
                 child.topLeft.x = parent.topLeft.x;
                 child.topLeft.y = parent.topLeft.y + parent.size/2;
                 child.size = parent.size/2; 
             	break; 
              default: //error checking. This should not occur 
                 System.out.println("Error in locating region");
                 System.exit(0); //exits
           }
     } 
	
}