//This is the structure we will be representing. 
/* It will hold a Node, to store the information of the current tree
it will have four children all of type quadtree 
an integer to hold the size of the tree 
a Coordinate to indicate the top left corner of a tree 
*/
public class QuadTree{

	Node quadNode = new Node(); //Here we will store the coordinates and value of the node 
	QuadTree child[] = new QuadTree[4]; //Each quadtree will have 4 quadtrees as children 
	public int size; //This will store the size of the tree
    Coordinate topLeft = new Coordinate(); //This will hold the Topleft corner of each quadtree. 
    //This will serve as a boundary 

	QuadTree(){ //empty constructor 
	  topLeft = new Coordinate(0,0);
      size = 0; 
	}

	public int FindRegion(Node current){//Function will identify appropraite region for the passed node 

		boolean northRegion = false; 
		boolean westRegion = false;

		if(current.point.y < topLeft.y + size/2){ //Condition will identify if the node belongs north of south 
			northRegion = true; 
		}

		if(current.point.x <  topLeft.x + size/2){ //Condition will identify if the node belongs west or south 
			westRegion = true; 
		}
		
		if(northRegion == true && westRegion == true){ //Each condition will return a number indicating the region 
			return 0; //NorthWest
		}else if(northRegion == true && westRegion == false){
            return 1; //NorthEast
		}else if(northRegion == false && westRegion == false){
			return 2; //SouthEast
		}else if (northRegion == false && westRegion == true){
			return 3; //SouthWest
		}

		return 4; //This should never be outputted. 
	}
}
