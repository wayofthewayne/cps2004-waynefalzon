public class BankLauncher{
	public static void main(String [] args){

		//THIS IS A DEMO RUN FEATURING ALL THE ASPECTS OF THE PROGRAM
	  
	 Administrator admin = new Administrator("Joshua", "Parker", 0); //Creating a administrator 

	 Client clint = new Client("Clint", "Wacker", 1); //Creating 2 clients 
	 Client wayne = new Client("Wayne", "Falzon", 2); //Note clint has id 1 and wayne has id 2 

		 
	 for(int i=0; i<3; i++){ //Clint opens 3 accounts
	 	clint.AskOpenAccount(clint, admin); //Note each account will have Id i (that is 0,1,2)
	 	System.out.println(); 
	 }

	 System.out.println("\n");

	 for(int i=0; i<2; i++){//Wayne opens 2 accounts 
	 	wayne.AskOpenAccount(wayne, admin); 
	 	System.out.println(); 
	 }

	 BankCard cCard = clint.ApplyCard(clint, admin); //Clint Applies for a card (for the purpose of the demo this should be on account 0)

     if(cCard != null){
     	System.out.println("\n 500 has been deposited to account 0 and 100 was withdrawn using the card"); 
     	cCard.DepositFromCard(clint.GetAccount(0),500); //Clint uses the card to deposite 500 and withdraw a 100 in account 0 
     	cCard.WithdrawFromCard(clint.GetAccount(0),100); 
     	System.out.println(); 
     }
     
     System.out.println("You are about to transfer 200 (this should be done from account 0 to 1)"); 
     clint.Transfer(clint, 200); //Clint will transfer 200 from an account 
     //For the case of the demo, this will be from account 0 to 1 

     System.out.println(); 

     clint.GetAccount(0).ShowLog(); //Display log s for both accounts (ID's 0 and 1)
     System.out.println();
     clint.GetAccount(1).ShowLog(); 

     for(int i=0; i<3; i++){
     	wayne.ApplyCard(wayne,admin); //wayne applies for 3 cards on account 0; 
     }

     System.out.println("\n"); 

     wayne.DisplayAccounts(wayne); //we display the accounts for wayne 

     System.out.println(); 

     wayne.ApplyCardRemoval(wayne,admin);//wayne apples to remove card no 1 from account 0 
     wayne.AskCloseAccount(wayne,admin);//wayne now removes account no 1

     System.out.println("\n"); 

     wayne.DisplayAccounts(wayne); //accounts are displayed to reflect the changes

     System.out.println(); 

     Client wayCli = wayne.JointAccount(wayne,clint); //Wayne and clint join an account (They should join account no 0)
     //Accounts should be of the same type and same currency; 


     if(wayCli != null){
        wayCli.DisplayAccounts(wayCli); //Display the joint account 
     }

	  
	}
}