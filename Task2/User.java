public class User{//User class 

	public String name; 
	public String surname; 
	private final int identificationKey; //Way of identifying the user 
	//Final as this should not be changed 



    /*name = the name of the user
      surname = the surname of the user
      id = a unique number to be able to identify each user 
    */
	public User(String name, String surname, int id){ //User constructor 
       this.name = name; //set name 
       this.surname = surname; //set surname  
       identificationKey = id; //set ID 
	}
    
    //RETURNS int, this will be the id of the the specific user 
	public int GetID(){ //Retreive ID
		return identificationKey; 
	}//useful for validation checks 

}