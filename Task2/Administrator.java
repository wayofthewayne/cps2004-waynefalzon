//Approves accounts, cards, Removes cards, accounts
public class Administrator extends User{ //Administrator which inherits from user 
    

    /*name = name of the admin 
      surname = surname of the admin 
      employeeID = the id of the employee such that he/she can be uniquely identified
    */
	public Administrator(String name, String surname, int employeeID){ //Admin constructor 
		super(name,surname,employeeID); //calls constructor from superclass (User)
	}
    

    //Function will open an account for a client 
    /*identificationKey = the client id to be able to verify a clients validity 
      client = the client which wishes to open the bank account
      initialBalance = the initial amount of money to be put into the account being opened 
      currency = the currency to be associated with the account 
      type = the type of account (savings/current)
    */
	public void OpenAccount(int identificationKey, Client client, int initialBalance, String currency, String type){
        if(identificationKey == client.GetID()){ //Check if client Id does exist 

        	client.AddAccount(client.accountCounter, initialBalance, currency, type); //Adds the account 
            
            System.out.println("Account opened"); //Display message 
            return; 
        } 

        	System.out.println("This client does not exist");//error message if wrong id is inputted  
	}

    //Function will close an account for a client 
    /*accountNo = the account Id which is to be deleted
      client = the Client which wants to deleted the account
    */
    public void CloseAccount(int accountNo, Client client){
         if(client.GetAccount(accountNo) != null){ //Checks if account exists 
            client.RemoveAccount(accountNo); //removes the account 
            client.accountCounter--; //decrement counter 
            System.out.println("Account removed");
         }else{
            System.out.println("Account does not exist!"); //error message 
         }
    }
    

    //Approves the request for a card 
    /*client = the client which wishes to have a bankcard
     accountNo = the account id which the card shall be associated with 
     RETURNS BankCard = returns the card for the user 
    */
    public BankCard ApproveCard(Client client, int accountNo){
        //Returns the card which has been added to the list of cards for the client under an account 
         return client.GetAccount(accountNo).AddCardToAccount( client.GetAccount(accountNo).cardCount );
         //Note the cardNo is set to as the same as cardCount. 
         //This sets the CardNo as unique without need for check; 
    }

    //Removes a card for a client from one of his accounts 
    /*client = the client which wishes to remove a card
      accountNo = the account Id from which the card shall be deleted
      cardNo = the card index to be deleted
    */
    public void RemoveCard(Client client, int accountNo, int cardNo){
        client.GetAccount(accountNo).RemoveCardFromAccount( cardNo); //Specific card is removed from a specific account 
    }
	
}