public class Current extends Account{ //Currents class which inherints from Account 


    /*amount = the initial amount of money to be deposited in the account
      currency = the currency to be associated with the current account
      id = the unique identifier for the current account 
    */
	public Current(double amount, String currency, int id){ //Current constructor 
		super(amount, currency, id); //Constuctor call from Account 
		this.type = "Current"; //Sets the type 
		super.TransactionLog("Thank you for opening your current account", ++tCounter); //Logs the action 
	}

    //Function will be used to deposit into current account 
	public void Deposit(double amount){
		super.TransactionLog(String.valueOf(amount) + " was stored in current account. ID: " + GetAccountId(), ++tCounter); 
		super.Deposit(amount);
	}
    

    //Function will be used to withdraw from current account 
	public void Withdraw(double amount){
		if(super.GetBalance() > amount){
		   super.TransactionLog(String.valueOf(amount) + " was withdrawn from current account. ID: " + GetAccountId(), ++tCounter);
		   super.Withdraw(amount); 
	    }else{
	    	System.out.println("Insufficient funds in current account"); 
	    }
	}
}