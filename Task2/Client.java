import java.util.*;
//Applies for accounts and cards 
public class Client extends User{ //Client/Regular User inherits from User 

	private List<Account> accounts = new ArrayList<Account>(); //Will store the list of accounts 
	public int accountCounter = 0;  //Will keep track of the amount of accounts 


       /*name = the name of the client 
         surname = the surname of the client 
         id = a unique identified for the client 
       */
       public Client(String name, String surname, int id){ //Client constructor 
            super(name,surname,id); //calls user constuctor 
       } 

       //Will be used to transfer between accounts of a user 
       /*client = the client which is transfering money 
         amount = the amount of money to be transferred 
       */
       public void Transfer(Client client, int amount){
           Scanner scan = new Scanner(System.in);

           //User asked to enter details 
           System.out.println(client.name + " from which account no would you like to remove money from?");
           int accountNo = scan.nextInt(); 
           System.out.println("To which account would you like to move money to ?");
           int toMoveto = scan.nextInt(); 

           if(client.accounts.get(accountNo) != null && client.accounts.get(toMoveto) != null){ //Checks if account exist 
               client.accounts.get(accountNo).Withdraw(amount); //Withdraws from an account 
               client.accounts.get(toMoveto).Deposit(amount); //Deposits to an account 
           }else{
            System.out.println("One of the accounts does not exists. Tranfer unable to complete");//error message 
           }

       }
       
       //Function will be used by client to try and open an account 
       /*client = the client which wishes to open account 
         admin = the admin which shall oversee/approve the procedure 
       */
       public void AskOpenAccount(Client client, Administrator admin){

              Scanner scan = new Scanner(System.in);//User is asked for information 

              System.out.println("Please enter your id");
              int id = scan.nextInt(); 

              System.out.println("Please enter initial balance");
              int initialBalance = scan.nextInt();

              String currency = new String();
              String type = new String(); 
              scan.nextLine(); //Buffer clear 

              System.out.println("Please enter the currency");
              currency = scan.nextLine(); 

              System.out.println("Please enter account type");
              type = scan.nextLine(); 

       	  System.out.println("Application to open bank account sent."); 
       	  admin.OpenAccount(id,client, initialBalance, currency, type);//Admin is used to open account 
       }

       //Will be used by user to try and close an account 
       /*client = the client which wishes to close an account
         admin = the administrator which shall oversee/complete the procedure 
       */
       public void AskCloseAccount(Client client, Administrator admin){

            Scanner scan = new Scanner(System.in); //User is asked to enter information 
            System.out.println("Enter the account you wish to delete"); 
            int accountNo = scan.nextInt(); 
       	System.out.println("Application to close account sent. Awaiting respone...");

            admin.CloseAccount(accountNo, client);//Account is closed by admin 
       }
       
       //Function will display a clients account along with all the account information 
       //client = the client for which his bank information shall be displayed 
       public void DisplayAccounts(Client client){
            System.out.println("Name: " + client.name + " Surname: " + client.surname + " ID: " +client.GetID()); 

       	System.out.println("Client Bank Account Details: \n"); 
       	   for(int i=0; i<accounts.size(); i++){
       	   	System.out.println("Account Id: " + accounts.get(i).GetAccountId() + "\nType: " + accounts.get(i).type + "\nCurrency: " + accounts.get(i).currency
                        + "\nBalance: " + accounts.get(i).GetBalance() ); //Shows account info 
                  accounts.get(i).ShowCardsInAccount(); //And all the cards with associated with an account 
       	   }
       }

       //Function will be used by the user to apply for a card 
       /*client = the client which wishes to apply for a card 
         admin = the administrator to approve the request 
         RETURNS BankCard = returns the bank card for which the client applied for 
       */
       public BankCard ApplyCard(Client client, Administrator admin){ // a card is returned so it can be used by the user to deposit/withdraw
            Scanner scan = new Scanner(System.in);//User is asked for info 

            System.out.println(client.name + " please enter the account u wish to add a card to: ");
            int accountNo = scan.nextInt(); 
            System.out.println("Application for card has been submitted");

        if(client.accounts.get(accountNo) != null){   //If account exists       
            System.out.println("Card has been approved by admin"); 
            return admin.ApproveCard(client, accountNo); //Admin gives card 
        }else{
            System.out.println("This account does not exist"); //display error message 
        }
            return null; //No card returned 
       }

       //User will apply here to remove a card 
       /*client = the client which wishes to remove a card from his account 
         admin = administrator to oversee the procedure 
       */
       public void ApplyCardRemoval(Client client, Administrator admin){

            Scanner scan = new Scanner(System.in); //User info is asked 
            System.out.println(client.name + " which card do you wish to remove");
            int cardNo = scan.nextInt(); 
            System.out.println("From which account shall the card be removed ?");
            int accountNo = scan.nextInt();

            System.out.println("Request to remove card has been submitted");
            
            if(client.accounts.get(accountNo).GetCard(cardNo) != null){ //If card exists
               admin.RemoveCard(client,accountNo,cardNo); //admin removes the card 
               System.out.println("Card has been removed.");
            }else{
                  System.out.println("Card to be removed does not exist"); //error message 
            }
       }

       /*Will merge two existing accounts into one (provided they meet the requirements). 
       Both previous seperate accounts will be removed. 
       A new client is created based on the previous two clients 
       */
       //Note it is assumed that funds from existing sole accounts are being transfered into the joint account and therefore closed. 
       /*client1 = one of the clients that wish to join an account 
         client2 = the other client which wishes to join an account 
         RETURNS Client, this client will have the joint account 
       */
      public Client JointAccount(Client client1, Client client2){
            Scanner scan = new Scanner (System.in); //Ask users for info 

            System.out.println(client1.name + " which account would you like to join ?"); 
            int accountNo1 = scan.nextInt(); 

            System.out.println(client2.name + " which account would you like to join? "); 
            int accountNo2 = scan.nextInt(); 

            if(client1.GetAccount(accountNo1) != null && client2.GetAccount(accountNo2) != null){//check if accounts exist 

                  String type1 = client1.GetAccount(accountNo1).type.toUpperCase(); 
                  String type2 = client2.GetAccount(accountNo2).type.toUpperCase(); 
                  String currency1 = client1.GetAccount(accountNo1).currency.toUpperCase();
                  String currency2 = client2.GetAccount(accountNo2).currency.toUpperCase(); 

                  if(type1.equals(type2) && currency1.equals(currency2)){//check if account type and currency match 

                        //Account balances are added up 
                        double newBalance = client1.GetAccount(accountNo1).GetBalance() + client2.GetAccount(accountNo2).GetBalance(); 
                        String type = client1.GetAccount(accountNo1).type; //type and currency retreived 
                        String currency = client1.GetAccount(accountNo1).currency; 

                        String sId = Integer.toString(client1.GetID()) + Integer.toString(client2.GetID());  
                        int id = Integer.valueOf(sId); //new Id is both previous Id s joined together
                        
                        Client client3 = new Client(client1.name + " " + client2.name, client1.surname + " " + client2.surname, id); 
                        //client 3 is created 
                        

                        client3.AddAccount(client3.accountCounter, newBalance, currency,type); //account is added 

                        if(client1.GetAccount(accountNo1).cardCount != 0){ //cards from account1 are added to new account 
                              for(int i=0; i<client1.GetAccount(accountNo1).cardCount; i++){
                                  client3.GetAccount(client3.accountCounter-1).AddCardToAccount(i); 
                                  //We need the minus one to avoid out of bounds index 
                              }
                        }
                        if(client2.GetAccount(accountNo2).cardCount != 0){ //cards from account2 are added to new account 
                              for(int i=client1.GetAccount(accountNo1).cardCount; i<client2.GetAccount(accountNo2).cardCount
                                    +client1.GetAccount(accountNo1).cardCount; i++){
                                 client3.GetAccount(client3.accountCounter-1).AddCardToAccount(i);
                              }
                        } //NOTE card No are arranged and will be display as the indexes 
                        //I.e if card account 1 has card 1 and 3 
                        //And account 2 has cards 0
                        //Account 3 will have cards 0,1,2

                        client1.RemoveAccount(accountNo1); //both accounts are removed 
                        client1.accountCounter--; 
                        client2.RemoveAccount(accountNo2); 
                        client2.accountCounter--; 


                        return client3; //Client is returned 

                  }else{
                        System.out.println("These accounts are confliciting. Make sure accounts share the same currency and type");
                        System.out.println("Unable to merge"); //error messages 
                  }

            }else{
                  System.out.println("Accounts do not exist");
            }

            return null; 

       }


//The functions below are simply to get information about the list of accounts due to its private nature 


      /*accountNo = account Id to be retreived
        RETURNS Account, the account required is returned 
      */
       public Account GetAccount(int accountNo){ //Will retreive an account
            return accounts.get(accountNo);
       }
       
       //RETURNS int, the amount of accounts   
       public int GetAccountSize(){ //Will get the amount of accounts a client has 
            return accounts.size(); 
       }
       

       //accountNo = the account Id which is removed
       public void RemoveAccount(int accountNo){//Will remove an account 
            accounts.remove(accountNo);
       }
        

       /*accountCounter = the counter which keeps track of the accounts 
         initialBalance = the balance which the account is to be opened with 
         currency = the currency to be associated with the account 
         type = whether the account is savings or current 
       */
       public void AddAccount(int accountCounter, double initialBalance, String currency ,String type){//Adds an account
            if((type.toUpperCase()).equals("CURRENT")){ //Identifies type (NOT case sensitive)

             accounts.add(accountCounter, new Current(initialBalance, currency, accountCounter)); //Adds to account list
             this.accountCounter++; //updates counter 
            }else if((type.toUpperCase()).equals("SAVINGS")){ //same logic but savings account 
             accounts.add(accountCounter, new Savings(initialBalance, currency, accountCounter)); 
             this.accountCounter++; //This is needed else the updated counter is not the one of the client but a parameter 
            }else{
             System.out.println("Please enter a valid type of account. I.e current or savings!");//Displays error message 
            }
       }

}