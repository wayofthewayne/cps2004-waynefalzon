import java.util.*; 

public class BankCard{ 

	private int cardNo; //Card number


    //The number of the card (for the context of the assignment this may also be the amount of cards in the account)
	BankCard(int cardNo){ //Card constuctor 
		this.cardNo = cardNo; //sets the number 
	}
    

    //RETURNS int, returns the card number for a card 
	public int GetCardNo(){ //Retrieves the card number 
		return cardNo; 
	}
    
    //Allow the client to deposit into the account from his card 
    /*account = the account in which the client wishes to deposit to 
      amount = the amount of money the client wishes to deposit 
    */
	public void DepositFromCard(Account account, int amount){
		account.Deposit(amount); //will call the deposit function on the account 
	}
    
    //Allow the client to withdraw from his account with his card 
    /*account = the account in which the client wishes to withdraw from
      amount = the amount of money the client wishes to withdraw
    */
	public void WithdrawFromCard(Account account, int amount){
		account.Withdraw(amount); //will call withdraw function on the account 
	}
}