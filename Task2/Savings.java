public class Savings extends Account{ //Savings class which inherits from Account 
    


    /* amount = the initial amount to be stored in the savings account
       currency = the currency of the savings account
       id = the unique identifier for the account 
    */
	public Savings(double amount, String currency, int id){ //Savings constructor 
		super(amount, currency, id); // Call constructor from Account 
		this.type = "Savings"; //set the type 
		super.TransactionLog("Thank you for opening your savings account. An interest rate of 0.0001% is given on the total amount per deposite.", ++tCounter);
	}
    
    //function will be used to deposit into a savings 
	public void Deposit(double amount){
		super.TransactionLog(String.valueOf(amount) + " was saved in savings. ID: " + GetAccountId(), ++tCounter); //Log the transaction 
		super.Deposit(amount); //Call deposit from Account 
		super.Deposit(super.GetBalance()*0.0001); //A small rate is added to the total after each deposit 
		//This is a feature of the savings (to seperate functionality from current account )
	}
    
    //Function will be used to take out money from an account 
	public void Withdraw(double amount){
		if(super.GetBalance() > amount){ //Check if money is available 
		super.Withdraw(amount); //Withdraw the amount 
		super.TransactionLog(String.valueOf(amount) + " was withdrawn from savings. ID: " + GetAccountId(), ++tCounter);//Log the transaction 
	    }else{
	    	System.out.println("Insufficient funds in savings account"); //Display error message 
	    }
	}
}

