import java.util.*; 

public class Account{

	private double balance=0; //Attribute to store the balance 
	private int accountId; //Will store the account Id

	private List<String> transactions = new ArrayList<String>(); //Will store the actions performed by account 
	//Set to private so as not to be tampared with 

	public String currency; 
	public int tCounter = 0; //Will serve as a counter for the transaction log 
	public String type; //will store whether the account is current or savings 

	private List <BankCard> cards = new ArrayList<BankCard>();  //A list of BankCards, which will store the cards 
	public int cardCount= 0; //will serve as a counter for the cards 
	 
    
    /*initialBalance = the first amount of money which shall be deposited in the account
      currency = indicating the type of currency for the bank account 
      id = an integer to uniquely identify the bank accounts
    */
	public Account(double initialBalance, String currency, int id){//Account constructor 
		String log = "Opened account with " + String.valueOf(initialBalance) + " in " + currency + " with id: " + id; 
		this.TransactionLog(log,0);  //Logging the message 
		accountId = id; //setting account id  (this should be ahead of deposit to effect id change before log)
		this.Deposit(initialBalance); //Setting the initial deposit 
		this.currency = currency; //setting currency 		
	}
    

    //amount = the amount to be put in the account 
	public void Deposit(double amount){ //Will be used to deposit in account 
		balance = balance + amount; 
	}

    //amount = the amount to be take out from an account
	public void Withdraw(double amount){ //Will be used to withdraw from account 
		balance = balance - amount; 
	}
    
    //RETURN double, balance of the account is returned 
	public double GetBalance(){ //Used to retreive balance 
        return balance; 
	} 
   

    /*log = the sentence to be put into the log 
      tCounter = a counter to keep track of the amount of logs 
    */
	public void TransactionLog(String log,int tCounter){ //Will log the transactions 
         transactions.add(tCounter, log);
	}
    
    //RETURN int, returns the ID associated with the account
	public int GetAccountId(){ //Will get the account Id 
		return accountId; 
	}

	public void ShowLog(){ //Display all the logs for a specific account 
		for(int i=0; i<transactions.size(); i++){
			System.out.println(transactions.get(i) + " ");
		}
	}
    
    //Adds a card to account
    /* cardNo = the number of the card (this can also be the amount of cards in the bank account)
       RETURN BankCard, returns the card so that it can be used by the client
    */
	public BankCard AddCardToAccount(int cardNo){  
       cards.add(cardCount, new BankCard(cardNo)); //Add new card to list of cards 
       TransactionLog("Card with Number: " + String.valueOf(cardNo) + " has been added to account with id: " + String.valueOf(accountId), ++tCounter); 
       cardCount++; //update counter 
       return cards.get(cardCount-1); //return the card 
	}
    
    //Removes card from an account 
    //cardCount = the card index to be deleted 
	public void RemoveCardFromAccount(int cardCount){
		cards.remove(cardCount); //remove card 
		cardCount--; //update counter and log the action 
		TransactionLog("Card with Number: " + String.valueOf(cardCount) + " has been removed from account with id: " + String.valueOf(accountId), ++tCounter);
	}


    //Show all the cards in an account 
	public void ShowCardsInAccount(){
		System.out.print("The cards are:"); 
		for(int i=0; i<cards.size(); i++){
          System.out.print(" CardNo: " + cards.get(i).GetCardNo());
		}
		System.out.println(); 
	}
    
    //Get a specific card 
    /*cardNo = the card index to be returned 
      RETURN BankCard, rturns the card that needed to be retreived
    */
	public BankCard GetCard(int cardNo){
		return cards.get(cardNo);
	}
}